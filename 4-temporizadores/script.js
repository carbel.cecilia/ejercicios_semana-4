"use strict";

let contadorIntervalo;//Declaramos la variable del intervalo de tiempo 1 y 5

let impresionIntervalo;//Declaramos el intervalo de imprimir para poder usarlo fuera

//proceso de conversion segundos-minutos-horas
function temporizador(tiempo){

  const segundos = tiempo % 60 //segundos a minutos

  const minutosTotales = Math.floor(tiempo/60)//minutos convertidos

  const minutos = minutosTotales % 60;//minutos a hora

  const horasTotales = Math.floor(minutosTotales / 60)// horas

  const horas = horasTotales % 24//horas a dia

  const dias = Math.floor(horasTotales/24)//dias

  console.log(`Han pasado ${dias} ${dias!==1? "días": "dia"}, ${horas} ${horas!==1? "horas": "hora"}, ${minutos} ${minutos!==1? "minutos": "minuto"} y ${segundos} ${segundos!==1? "segundos": "segundo"}.`)
}


function temporizadorUnoYCinco(){
  
  let counter = 0;//Inicia contador a 0

  contadorIntervalo = setInterval(()=>{//Iniciamos el intervalo del tiempo

    counter++ //contador aumenta 1 en 1

  }, 1000) //1 segundo

  impresionIntervalo = setInterval(() => { //intervalo imprimir declarada.
   
    temporizador(counter)}, //funcion temporizador llama a contador

    5000)//5 segundos
}

temporizadorUnoYCinco();

//stop temporizador