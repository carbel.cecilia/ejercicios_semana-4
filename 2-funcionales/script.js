"use strict";

// puntuaciones
const puntuaciones = [
  {
    equipo: "Toros Negros",
    puntos: [1, 3, 4, 2, 10, 8],
  },
  {
    equipo: "Amanecer Dorado",
    puntos: [8, 5, 2, 4, 7, 5, 3],
  },
  {
    equipo: "Águilas Plateadas",
    puntos: [5, 8, 3, 2, 5, 3],
  },
  {
    equipo: "Leones Carmesí",
    puntos: [5, 4, 3, 1, 2, 3, 4],
  },
  {
    equipo: "Rosas Azules",
    puntos: [2, 1, 3, 1, 4, 3, 4],
  },
  {
    equipo: "Mantis Verdes",
    puntos: [1, 4, 5, 1, 3],
  },
  {
    equipo: "Ciervos Celestes",
    puntos: [3, 5, 1, 1],
  },
  {
    equipo: "Pavos Reales Coral",
    puntos: [2, 3, 2, 1, 4, 3],
  },
  {
    equipo: "Orcas Moradas",
    puntos: [2, 3, 3, 4],
  },
];

function puntajesEquipos(equipos) {
  const totalEquipo = equipos.map(arrayEquipo => {
    const { equipo, puntos } = arrayEquipo;// array de nombre del equipo y los puntajes totales
    console.log(equipo, puntos)

    const total = puntos.reduce((a, b) => a + b)//reduce- sumara valor previo y valor actual de todo el array regresando un valor unico 
    console.log(total)
    return{equipo, total}
  })

  //se agrega metetod sort para ordenar array ya que sin este me da un puntaje mayor pero en razón del orden del array, no en razón del orden del puntaje.
  totalEquipo.sort((a, b) => a.total - b.total)
  //console.log(totalEquipo)
  //se agrega el peor y mejor
  const mejorEquipo = totalEquipo[0];
  const peorEquipo = totalEquipo[totalEquipo.length - 1]

  //imprimir pot consola
  console.log(`El peor equipo es ${peorEquipo.equipo} con un total de ${peorEquipo.total} puntos`)
  console.log(`El mejor equipo es ${mejorEquipo.equipo} con un total de ${mejorEquipo.total} puntos`)
  


}
puntajesEquipos(puntuaciones);