"use strict";

//Promesa de la edad. Devuelve un número del 0 al 100, tardando entre 0 y 1 segundos
const agePromise = new Promise((resolve, reject) => setTimeout(() => resolve(Math.round(Math.random() * 100)), Math.random() * 1000)) 


function promesaEdad(edad){
 console.log(edad)
  return new Promise((resolve, reject) => {
   
    if(edad < 18){ //Si edad es menor a 18 la promesa devuelve "edad es menor".
        console.log("menor")
      reject("Es menor")
    }
   
    if(edad % 2 === 0){ //Si edad par, la promesa devuelve "edad es par".
      setTimeout(()=>{
        console.log("es par")
        resolve("Edad es par")
      }, 1000)
    } else {
      setTimeout(()=>{ //Si edad impar, la promesa devuleve "edad es impar".
        console.log("impar")
        resolve("Edad es impar")
      }, 2000)

    }

  })
}
promesaEdad(17)


